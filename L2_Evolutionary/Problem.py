class Problem:
    def __init__(self, input_file: str):
        self.set = []  # The given set S
        # This set must be partitioned in two blocks, D0 and D1
        # such that each block does not contain any subset in the forbidden subset list

        self.subsets = []  # The forbidden subsets
        # A list of subsets which should not be found in either partition

        self.parse_input(input_file)

    def parse_input(self, input_file: str):
        with open(input_file, 'r') as f:
            lines = f.readlines()
            self.set = [int(x) for x in lines[0].split() if x.isdigit()]
            for line in lines[1:]:
                subset = [int(x) for x in line.split() if x.isdigit()]
                self.subsets.append(subset)
