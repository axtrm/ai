from Individual import Individual
from Problem import Problem
from Algorithm import Algorithm


class Application:

    def __init__(self, config_file, data_file):
        self.config_file = config_file
        self.data_file = data_file

    def main(self):
        p = Problem(self.data_file)
        alg = Algorithm(self.config_file)

        result = alg.run(p)

        if result is None:
            print("Could not find result.")
            return

        self.pretty_print(result, p)
        alg.statistics()

    @staticmethod
    def pretty_print(result: dict, p: Problem):
        key_list = list(result.keys())  # A list of Individuals
        chosen_candidate = key_list[0]  # Choose a random individual

        if not isinstance(chosen_candidate, Individual):
            raise Exception("chosen_candidate is not of type Individual")

        print("A possible solution (f = {}):".format(chosen_candidate.fitness(p)))
        print("\tSet: {}".format(p.set))
        print("\tBlock 0: {}".format(chosen_candidate.get_block0(p.set)))
        print("\tBlock 1: {}".format(chosen_candidate.get_block1(p.set)))

        print()  # For formatting

        for (k, v) in result.items():
            print("{} -> f = {}".format(k, v))


if __name__ == "__main__":
    app = Application("data/params.in", "data/data00.in")
    app.main()
