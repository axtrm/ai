import configparser
from random import randint

import Problem
from Individual import Individual
from Population import Population


class Algorithm:

    def __init__(self, config_file: str):
        self.mutation_pb = 0.0
        self.crossover_mutation_pb = 0.0
        self.population_size = 0
        self.no_generations = 0
        self.population = None

        self.fitness_values = []  # Collects population overall fitness values
        # After every iteration, a value is added
        # statistics() then creates a plot

        self.parse_config(config_file)
        self.initialize()  # Creates all the necessary structures

    def initialize(self):
        self.population = Population(self.population_size)

    def parse_config(self, config_file: str):
        config = configparser.ConfigParser()
        config.read(config_file)

        if 'constants' not in config:
            raise Exception("Invalid config file '{}'.".format(config_file))

        constants = config['constants']
        self.mutation_pb = float(constants['mutation_pb'])
        self.crossover_mutation_pb = float(constants['crossover_mutation_pb'])
        self.population_size = int(constants['population_size'])
        self.no_generations = int(constants['no_generations'])

    def run(self, p: Problem) -> dict:
        for i in range(0, self.no_generations):
            self.iteration(p)

        return {x: x.fitness(p) for x in self.population.population}

    def iteration(self, p: Problem):
        # Compute fitness of each population
        overall_fitness = self.population.get_overall_fitness(p)
        self.fitness_values.append(overall_fitness)

        p1 = randint(0, self.population_size - 1)
        p2 = randint(0, self.population_size - 1)
        if p1 != p2:
            child = Individual.crossover(self.population.get(p1),
                                         self.population.get(p2),
                                         self.crossover_mutation_pb)

            child.mutate(self.mutation_pb)

            fchild = child.fitness(p)
            fp1 = self.population.get(p1).fitness(p)
            fp2 = self.population.get(p2).fitness(p)

            if fchild < fp1:
                self.population.replace_at(p1, child)
            elif fchild < fp2:
                self.population.replace_at(p2, child)

    def statistics(self):
        import matplotlib.pyplot as plt
        plt.plot(list(range(0, self.no_generations)), self.fitness_values, 'ro')
        plt.ylabel('Population fitness')
        plt.xlabel('Generations')
        plt.show()
