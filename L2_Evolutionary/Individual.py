from random import randint, random

from bitarray import bitarray

from Problem import Problem


class Individual:

    def __init__(self, size: int):
        self.size = size
        self.mask = bitarray(self.size * bitarray('0'))
        self.generate()

    def generate(self):
        bits = [randint(0, 1) for x in range(0, self.size)]
        self.mask = bitarray(bits)

    def fitness(self, p: Problem) -> float:
        block0 = set(self.get_block0(p.set))
        block1 = set(self.get_block1(p.set))

        collisions = 0
        no_subsets = len(p.subsets)

        for subset in p.subsets:
            s = set(subset)
            if s.issubset(block0):
                collisions += 1
                #  print("DEBUG: block0 ({}) contains {}.".format(block0, s))
            if s.issubset(block1):
                collisions += 1
                #  print("DEBUG: block1 ({}) contains {}.".format(block1, s))

        return float(collisions / no_subsets)

    def get_block0(self, s: list) -> list:
        not_mask = bitarray(self.mask)
        not_mask.invert()
        return self.get_block(not_mask, s)

    def get_block1(self, s: list) -> list:
        return self.get_block(self.mask, s)

    @staticmethod
    def get_block(mask: bitarray, s: list) -> list:
        block = []
        for i in range(0, mask.length()):
            if mask[i]:
                block += [s[i]]
        return block

    def mutate(self, probability: float):
        r = random()
        if r < probability:
            i = randint(0, self.mask.length() - 1)
            self.mask[i] = 1 - self.mask[i]

    @staticmethod
    def crossover(p1, p2, probability):
        n = p1.mask.length()
        child = Individual(p1.size)
        child.mask = bitarray(p1.mask)

        for i in range(0, n):
            if random() < probability:
                child.mask[i] = p2.mask[i]
        return child

    def __repr__(self):
        return "I({})".format(self.mask.to01())
