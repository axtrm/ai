from Individual import Individual
from Problem import Problem


class Population:
    def __init__(self, size: int):
        self.size = size
        self.population = [Individual(size) for i in range(0, self.size)]
        # self.overall_fitness = -1.0  # -1.0 chosen only for debugging

    def set_population(self, lst: list):
        self.population = lst

    def get_population(self) -> list:
        return self.population

    def get_size(self) -> int:
        return len(self.population)

    def replace_at(self, i: int, idv: Individual):
        self.population[i] = idv

    def get(self, i: int):
        if 0 <= i < len(self.population):
            return self.population[i]
        return None

    def get_fitness_of(self, p: Problem, i: int):
        if 0 <= i < len(self.population):
            if isinstance(self.population[i], Individual):
                return self.population[i].fitness(p)

    def get_overall_fitness(self, p: Problem) -> float:
        f = 0.0

        for individual in self.population:
            f += float(individual.fitness(p))

        return float(f) / len(self.population)

    def __str__(self):
        return "P({})".format(self.population)
