from unittest import TestCase

from bitarray import bitarray

from Individual import Individual
from Problem import Problem


class TestIndividual(TestCase):
    def test_generate(self):
        i = Individual(10)
        print(i.mask)

    def test_get_block0(self):
        i = Individual(10)
        s = list(range(1, 10 + 1))
        print(s)
        print(i.get_block0(s))
        print(i.get_block1(s))

    def test_fitness(self):
        i = Individual(10)
        p = Problem("../data/data00.in")
        print(p.set)
        print(p.subsets)
        print(i.get_block0(p.set))
        print(i.get_block1(p.set))
        print(i.fitness(p))

    def test_mutate(self):
        i = Individual(10)
        print(i.mask)
        i.mutate(1)
        print(i.mask)

    def test_crossover(self):
        i1 = Individual(5)
        i2 = Individual(5)
        i1.mask = bitarray([True, True, False, False, False])
        i2.mask = bitarray([False, False, True, True, True])
        c = Individual.crossover(i1, i2, 0.15)
        print(i1)
        print(i2)
        print(c)
