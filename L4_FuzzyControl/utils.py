import sys


def err(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def log_fuzz(message: str, fuzz: dict):
    print(message)
    for k, v in fuzz.items():
        print("\t{1} for {0}".format(k, v))


# Trapezoidal / Triangular Functions
def trapezoidal(x, a, b, c, d):
    return max(0, min((x - a) / (b - a), 1, (d - x) / (d - c)))


def triangular(x, a, b, c):
    return max(0, min((x - a) / (b - a), (c - x) / (c - b)))
