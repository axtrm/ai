from observer import Observer
from utils import trapezoidal, triangular, log_fuzz


class Fuzzifier:

    def __init__(self):
        self.temperature_fuzz = None
        self.humidity_fuzz = None

    def fuzzify(self, observer: Observer):
        self.temperature_fuzz = self.fuzzify_temperature(observer.temperature)
        self.humidity_fuzz = self.fuzzify_humidity(observer.humidity)
        log_fuzz("Temperature fuzz:", self.temperature_fuzz)
        log_fuzz("Humidity fuzz:", self.humidity_fuzz)

    @staticmethod
    def fuzzify_temperature(temperature):
        return {
            "very cold": trapezoidal(temperature, -100, -30, -20, 5),
            "cold": triangular(temperature, -5, 0, 10),
            "normal": trapezoidal(temperature, 5, 10, 15, 20),
            "warm": triangular(temperature, 15, 20, 25),
            "very warm": trapezoidal(temperature, 25, 30, 35, 100)
        }

    @staticmethod
    def fuzzify_humidity(humidity):
        return {
            "dry": triangular(humidity, -1000, 0, 50),
            "normal": triangular(humidity, 0, 50, 100),
            "wet": triangular(humidity, 50, 100, 1000)
        }

    # def log(self):
    #     print("Temperature fuzz:")
    #     for state in self.temperature_fuzz:
    #         print("{1} of {0}".format(state, self.temperature_fuzz[state]))
    #
    #     print("Humidity fuzz:")
    #     for state in self.humidity_fuzz:
    #         print("{1} of {0}".format(state, self.humidity_fuzz[state]))
