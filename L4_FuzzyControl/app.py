from controller import Controller
from defuzzifier import Defuzzifier
from fuzzifier import Fuzzifier
from observer import Observer


def run():
    o = Observer()
    f = Fuzzifier()
    c = Controller()
    d = Defuzzifier()

    o.observe()
    f.fuzzify(o)
    c.compute(f)
    d.defuzzify(c)

    print("Crisp time:", d.crisp_time)


if __name__ == '__main__':
    run()
