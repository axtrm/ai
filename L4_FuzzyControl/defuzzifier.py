from controller import Controller
from utils import triangular


class Defuzzifier:
    def __init__(self):
        self.crisp_time = ""

    def get_crisp_time(self):
        pass

    def defuzzify(self, controller: Controller):
        """
        Defuzzify using centroid area method.
        """
        sample = {}

        for x in range(0, 100 + 1):
            mu = {
                "short": min(controller.time_for("short"), triangular(x, -1000, 0, 50)),
                "medium": min(controller.time_for("medium"), triangular(x, 0, 50, 100)),
                "long": min(controller.time_for("long"), triangular(x, 50, 100, 1000))
            }

            mu = max(mu.values())

            sample[mu] = x * mu

        self.crisp_time = sum(sample.values()) / sum(sample.keys())
