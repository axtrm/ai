from utils import err


class Observer:

    def __init__(self):
        self.temperature = 0.0
        self.humidity = 0.0

    def observe(self):
        self.temperature = float(input("Temperature [-30, 35]:"))
        self.humidity = float(input("Humidity [0. 100]:"))

        if not self.validate():
            err("Some given value(s) are out of valid ranges.")
            self.observe()

    def validate(self):
        return -30 < self.temperature < 35 and 0 < self.humidity < 100
