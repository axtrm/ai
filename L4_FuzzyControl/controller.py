from fuzzifier import Fuzzifier
from utils import log_fuzz


class Controller:

    def __init__(self):
        self.rules = {
            "very cold": {"wet": "short", "normal": "short", "dry": "medium"},
            "cold": {"wet": "short", "normal": "medium", "dry": "long"},
            "normal": {"wet": "short", "normal": "medium", "dry": "long"},
            "warm": {"wet": "short", "normal": "medium", "dry": "long"},
            "very warm": {"wet": "medium", "normal": "long", "dry": "long"}
        }

        self.state_to_time = {"short": 0, "medium": 0, "long": 0}

    def time_for(self, state):
        return self.state_to_time[state]

    def compute(self, fuzzifier: Fuzzifier):
        rules = self.rules
        state_to_time = self.state_to_time

        for t in ["very cold", "cold", "normal", "warm", "very warm"]:
            for h in ["dry", "normal", "wet"]:
                state = rules[t][h]
                time = min(fuzzifier.temperature_fuzz[t], fuzzifier.humidity_fuzz[h])
                state_to_time[state] = max(state_to_time[state], time)

        log_fuzz("Computed time: ", self.state_to_time)
