import random
import uuid
from math import sin, cos, tan, log

BINARY_OPERATIONS = ["+", "-", "*", "/"]
UNARY_OPERATIONS = ["-", "sin", "cos", "tan", "ln", "ln1px", "sin1mx", "^2", "k"]


class ExpressionException(Exception):
    pass


class Node:

    def __init__(self, left=None, right=None):
        self.left = left
        self.right = right
        self.uid = uuid.uuid4().hex  # Give each node a unique identifier

    def __eq__(self, other):
        if not isinstance(other, Node):
            return False
        return self.uid == other.uid

    def evaluate(self, terms: tuple) -> float:
        pass

    def mutate(self):
        pass


class EmptyNode(Node):

    def __init__(self):
        super().__init__(left=None, right=None)

    def evaluate(self, terms: tuple):
        raise ExpressionException("Can't evaluate an empty node.")

    def mutate(self):
        raise ExpressionException("Can't mutate an empty node.")


class BinaryOp(Node):

    def __init__(self, operation: str, left=None, right=None):
        super().__init__(left, right)
        self.operation = operation

    def evaluate(self, terms: tuple) -> float:
        op = self.operation
        lhs = self.left.evaluate(terms)
        rhs = self.right.evaluate(terms)

        if op == "+":
            return lhs + rhs
        elif op == "-":
            return lhs - rhs
        elif op == "*":
            return lhs * rhs
        elif op == "/":
            if rhs == 0:  # Safeguard against division by 0
                return 0
            return lhs / rhs

    def mutate(self):
        self.operation = random.choice(BINARY_OPERATIONS)

    def __repr__(self):
        return "({0} {1} {2})".format(self.left, self.operation, self.right)


class ValueNode(Node):
    def __init__(self, index: int):
        super().__init__(left=None, right=None)
        self.index = index

    def evaluate(self, terms: tuple) -> float:
        if 0 <= self.index < len(terms):
            return float(terms[self.index])

        raise ExpressionException("Invalid value index: {0}.".format(self.index))

    def mutate(self):
        pass

    def __repr__(self):
        return "t{0}".format(self.index)


class UnaryOp(Node):
    def __init__(self, operation: str, child: Node):
        super().__init__(left=child, right=None)  # Initialize left with its child node, by convention
        self.operation = operation
        self.child = child
        self.k = random.uniform(1, 10)  # Every unary operation seeds a random constant

    def mutate(self):
        self.k = random.uniform(1, 10)
        self.operation = random.choice(UNARY_OPERATIONS)

    def evaluate(self, terms: tuple) -> float:
        x = self.child.evaluate(terms)
        op = self.operation

        y = 0.0

        try:
            y = self.compute(x, op)
        except ValueError:
            pass

        return y

    def compute(self, x: float, op: str):
        if op == "-":
            return -x
        elif op == "sin":
            return float(sin(x))
        elif op == "cos":
            return float(cos(x))
        elif op == "tan":
            return float(tan(x))
        elif op == "ln":
            return float(log(x))
        elif op == "ln1px":
            return float(log(1 + x))
        elif op == "sin1mx":
            return float(sin(1 - x))
        elif op == "k":
            return self.k * x
        elif op == "^2":
            return x * x

    def __repr__(self):
        if self.operation == "k":
            return "{0}*{1}".format(self.k, self.child)
        elif self.operation == "^2":
            return "{0}^2".format(self.child)
        elif self.operation in ["+", "-"]:
            return "{0}{1}".format(self.operation, self.child)

        return "{0}({1})".format(self.operation, self.child)
