import random

from expression.node import ValueNode, UnaryOp, BinaryOp, EmptyNode, Node, UNARY_OPERATIONS, BINARY_OPERATIONS


def pick(domain: list):
    """
    Picks from a list and removes the picked element.
    :param domain: a list of elements
    :returns the picked element or None if the list is empty
    """
    if not domain:
        return None

    random.shuffle(domain)
    return domain.pop()


def apply_unary(x):
    if x is None:
        return None

    return UnaryOp(random.choice(UNARY_OPERATIONS), x)


def apply_binary(x, y):
    return BinaryOp(random.choice(BINARY_OPERATIONS), left=x, right=y)


def generate_expression(t: int) -> Node:
    """
    Generate an expression in a bottom-up fashion.
    :param t: value tuple size
    :return:
    """
    if t == 0:
        return EmptyNode()

    return bottom_up([ValueNode(k) for k in range(t)])


def bottom_up(level):
    if len(level) == 1:
        return level[0]

    next_level = []

    while level:
        x, y = pick(level), pick(level)
        x, y = apply_unary(x), apply_unary(y)

        if x and y:
            xy = apply_binary(x, y)
            next_level.append(xy)
        else:
            next_level.append(x)

    return bottom_up(next_level)
