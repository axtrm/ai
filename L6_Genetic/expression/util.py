from expression.node import Node


def node_count(root: Node):
    if root is None:
        return 0

    count = 0
    q = [root]

    while q:
        count += 1
        node = q.pop()

        if node.left is not None:
            q.append(node.left)

        if node.right is not None:
            q.append(node.right)

    return count


def get_node(root: Node, i: int) -> Node:
    """
    Search the tree in a predetermined way and retrieve the i-th node.
    :param root: root of the tree
    :param i: number of the node we will return
    :return: the i-th node or None
    """
    index = 0
    q = [root]

    while q:
        node = q.pop()

        if index == i:
            return node

        index += 1

        if node.left is not None:
            q.append(node.left)

        if node.right is not None:
            q.append(node.right)


def replace_node(root: Node, target: Node, replacement: Node):
    """
    Search for a target node and swap with replacement.
    """
    q = [root]

    while q:
        node = q.pop()

        if node.left and node.left == target:
            node.left = replacement
            return root

        if node.right and node.right == target:
            node.right = replacement
            return root

        if node.left:
            q.append(node.left)

        if node.right:
            q.append(node.right)

    return root
