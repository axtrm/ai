import copy
import random

from expression.rg import generate_expression
from expression.util import node_count, get_node, replace_node
from problem import Problem


class Individual:

    def __init__(self, size: int, expression=None):
        self.size = size
        self.expression = expression or generate_expression(self.size)

    def fitness(self, p: Problem) -> float:
        fitness = 0.0

        for input_values, expected_output in zip(p.inputs, p.outputs):
            output = self.expression.evaluate(tuple(input_values))
            fitness += abs(output - expected_output)

        return 1 / (1 + fitness)

    def mutate(self, probability: float):
        """
        Pick an arbitrary node and mutate it.
        :param probability: the probability of mutation
        """
        if random.random() > probability:
            return

        x = random.randint(0, node_count(self.expression) - 1)  # a random valid node index in the tree
        get_node(self.expression, x).mutate()

    @staticmethod
    def crossover(p1, p2, probability: float):
        """
        Pick two sub-expressions from p1 and p2 and swap them.
        :param p1, p2: individuals
        :param probability: the probability of mutation
        """
        if random.random() > probability:
            return p1, p2

        size = p1.size
        x, y = p1.expression, p2.expression

        subtree_x = copy.deepcopy(
            get_node(x, random.randint(1, node_count(x) - 1)))  # Start from 1 because swapping roots is useless

        subtree_y = copy.deepcopy(get_node(y, random.randint(1, node_count(y) - 1)))

        alpha = replace_node(x, subtree_x, subtree_y)
        beta = replace_node(y, subtree_y, subtree_x)

        return Individual(size, expression=alpha), Individual(size, expression=beta)

    def __repr__(self):
        return "I({})".format(self.expression)
