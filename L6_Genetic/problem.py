class Problem:
    def __init__(self, input_file: str):
        self.inputs = []
        self.outputs = []
        self.size = 0

        self.parse_input(input_file)

    def parse_input(self, input_file: str):
        with open(input_file, 'r') as f:
            lines = f.readlines()
            for line in lines[1:]:
                values = line.split(',')
                input_set = [float(x) for x in values[1:-1]]
                output = float(values[-1])

                self.inputs.append(input_set)
                self.outputs.append(output)

        self.size = len(self.inputs[0])
