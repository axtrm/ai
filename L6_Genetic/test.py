from algorithm import Algorithm
from individual import Individual
from problem import Problem


def pretty_print(result: dict, p: Problem):
    key_list = list(result.keys())  # A list of Individuals
    chosen_candidate = key_list[0]  # Choose a random individual

    if not isinstance(chosen_candidate, Individual):
        raise Exception("chosen_candidate is not of type Individual")

    print("A possible solution (f = {}):".format(chosen_candidate.fitness(p)))

    print()  # For formatting

    for (k, v) in result.items():
        print("{} -> f = {}".format(k, v))


def test():
    p = Problem('../data/slump_test.data')
    alg = Algorithm('../data/params.in', p)

    result = alg.run(p)

    if result is None:
        print("Could not find result.")
        return

    pretty_print(result, p)
    alg.statistics()


if __name__ == '__main__':
    test()
