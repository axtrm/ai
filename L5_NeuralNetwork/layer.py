from node import Node


class Layer:
    def __init__(self, size: int, num_inputs: int):
        """
        Initializes a layer of the neural network.

        :param size: the number of nodes this layer contains
        :param num_inputs: the number of inputs on each node
        """
        self.size = size
        self.nodes = [Node(num_inputs) for k in range(self.size)]

    def outputs(self):
        return [node.output for node in self.nodes]

    def __iter__(self):
        return iter(self.nodes)
