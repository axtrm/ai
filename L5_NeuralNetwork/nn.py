from math import exp

from layer import Layer


class Network:

    def __init__(self, num_inputs: int, num_outputs: int, num_hidden_layers: int, hidden_layer_size: int):
        self.num_inputs = num_inputs
        self.num_outputs = num_outputs

        # Parameters for hidden layers
        self.num_hidden_layers = num_hidden_layers
        self.hidden_layer_size = hidden_layer_size

        # Create layers
        self.layers = []
        self.create_layers()

        # Learning parameters
        self.learning_rate = 0.0
        self.epoch_limit = 0
        self.success_rate = 0.0

    def create_layers(self):
        self.layers += [Layer(self.num_inputs, 0)]
        self.layers += [Layer(self.hidden_layer_size, self.num_inputs)]
        self.layers += [Layer(self.hidden_layer_size, self.hidden_layer_size) for k in
                        range(self.num_hidden_layers - 1)]
        self.layers += [Layer(self.num_outputs, self.hidden_layer_size)]

        print("Creating layers:")
        print("\t{0} neurons on the input layer.".format(self.num_inputs))
        print("\t{0} neurons on the output layer.".format(self.num_outputs))
        print("\t{0} hidden layers with {1} neurons on each hidden layer.".format(self.num_hidden_layers,
                                                                                  self.hidden_layer_size))

    def set_learning_parameters(self, learning_rate: float, epoch_limit: int, success_rate: float):
        self.learning_rate = learning_rate
        self.epoch_limit = epoch_limit
        self.success_rate = success_rate

    def activate(self, inputs):
        # Set the values on the input layer
        neuron_index = 0
        for neuron in self.layers[0]:
            neuron.output = inputs[neuron_index]
            neuron_index += 1

        # Activate all the neurons on the hidden layers and the output layer
        layer_index = 1
        for layer in self.layers[1:]:
            for neuron in layer:
                prev_layer = self.layers[layer_index - 1]
                neuron.activate(prev_layer.outputs())
            layer_index += 1

    def backprop(self, errors):
        # Set errors on the output layer
        neuron_index = 0
        for neuron in self.layers[-1]:
            neuron.set_error(errors[neuron_index])
            neuron_index += 1

        # Set errors on the hidden layers and the input layer
        layer_index = len(self.layers) - 2
        for layer in self.layers[::-1][1:]:
            next_layer = self.layers[layer_index + 1]
            neuron_index = 0
            for neuron in layer:
                sum_errors = sum([neuron.weights[neuron_index] * neuron.error for neuron in next_layer])
                neuron.set_error(sum_errors)
                neuron_index += 1
            layer_index -= 1

        # Update weights
        layer_index = 1
        for layer in self.layers[1:]:
            prev_layer = self.layers[layer_index - 1]
            for neuron in layer:
                for i in range(neuron.num_inputs):
                    delta = self.learning_rate * neuron.error * prev_layer.nodes[i].output
                    neuron.weights[i] += delta
            layer_index += 1

    def compute_transformed_outputs(self):
        # Use softmax to transform the outputs into probabilities
        sum_exp = sum(exp(node.output) for node in self.layers[-1])
        return [exp(node.output) / sum_exp for node in self.layers[-1]]

    def compute_prediction(self):
        num_labels = self.layers[-1].size
        transformed_outputs = self.compute_transformed_outputs()

        # Compute label as the one with the highest probability
        computed_label = 0
        max_prob = transformed_outputs[0]
        for i in range(num_labels):
            if transformed_outputs[i] > max_prob:
                computed_label = i
                max_prob = transformed_outputs[i]
        return computed_label

    def compute_errors(self, target):
        num_labels = self.layers[-1].size
        transformed_outputs = self.compute_transformed_outputs()

        # Compute errors
        errors = [-e for e in transformed_outputs]
        errors[target] += 1

        # Compute global error
        computed_label = self.compute_prediction()
        is_correct = (1 if computed_label == target else 0)
        return is_correct, errors

    def compute_success(self, is_corrects):
        if len(is_corrects) < 1:
            return None
        total_corrects = sum(is_corrects)
        success = total_corrects / len(is_corrects)
        return success

    def learn(self, input_data, output_data):
        num_data = len(input_data)
        stop_condition = False
        epoch = 0
        print("Starting to learn...")
        while not stop_condition and epoch < self.epoch_limit:
            is_corrects = []
            for data_index in range(num_data):
                self.activate(input_data[data_index])
                is_correct, errors = self.compute_errors(output_data[data_index])
                self.backprop(errors)
                is_corrects.append(is_correct)
            success = self.compute_success(is_corrects)
            stop_condition = success > self.success_rate
            print("Epoch #{0}, success on training data: {1}".format(epoch, success))
            epoch += 1
        print("Enough learning!")
