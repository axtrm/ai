from random import random

from functions import sigmoid


class Node:

    def __init__(self, num_inputs: int):
        self.num_inputs = num_inputs or 0
        self.weights = [random() for k in range(self.num_inputs)]
        self.output = 0
        self.error = 0

    def activate(self, values):
        z = sum([values[i] * self.weights[i] for i in range(self.num_inputs)])
        self.output = sigmoid(z)

    def set_error(self, error):
        self.error = error * self.output * (1 - self.output)
