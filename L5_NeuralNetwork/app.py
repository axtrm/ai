from statistics import mean, stdev

import mnist_loader
import matplotlib.pyplot as plt
import nn


def read_data(filename):
    input_data = []
    output_data = []
    f = open(filename)
    raw_values = []
    for line in f:
        raw_values.append(list(map(float, line.split())))
    # shuffle(raw_values)
    for values in raw_values:
        input_data.append(values[:-1])
        output_data.append(values[-1])
    output_data = list(map(int, output_data))
    return input_data, output_data


def normalize_data(data):
    if len(data) < 1:
        return

    num_features = len(data[0])
    for feature_index in range(num_features):
        miu = mean([entry[feature_index] for entry in data])
        sigma = stdev([entry[feature_index] for entry in data])
        maxx = max([entry[feature_index] for entry in data])
        minn = min([entry[feature_index] for entry in data])
        for i in range(len(data)):
            if maxx != minn:
                data[i][feature_index] = (data[i][feature_index] - minn) / (maxx - minn) * 2 - 1
            else:
                data[i][feature_index] = 0


def main():
    filename = 'medical.txt'
    input_data, output_data = read_data(filename)
    input_train_data = input_data[:2000]
    output_train_data = output_data[:2000]

    normalize_data(input_train_data)

    num_features = len(input_data[0])
    num_labels = max(output_data) + 1

    network = nn.Network(num_features, num_labels, 2, 10)
    network.set_learning_parameters(0.2, 1000, 0.95)
    network.learn(input_train_data, output_train_data)


if __name__ == '__main__':
    main()
