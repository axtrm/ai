class Problem:

    def __init__(self, input_file: str):
        # 2D array describing whether there is a path from node i to node j in our graph
        self.dimension = 0  # Number of nodes, relevant for particle size
        self.can_reach = {}

        self.parse_input(input_file)

    def parse_input(self, input_file: str):
        """
        Parses the graph from file

        """

        lines = open(input_file, 'r').readlines()

        num_nodes = int(lines[0])
        neighbour_lists = {node: [] for node in range(num_nodes)}

        # Populate the neighbour list, using data from file
        for line in lines[1:]:
            x, y = map(int, line.strip().split())
            neighbour_lists[x].append(y)
            neighbour_lists[y].append(x)

        self.dimension = num_nodes
        self.compute_paths(num_nodes, neighbour_lists)

    def compute_paths(self, num_nodes, neighbour_lists):
        """
        Pre-computes which nodes can be visited from which other nodes.

        """
        # Initialize our 2D array, now that we know the number of nodes
        self.can_reach = [[False for i in range(num_nodes)] for j in range(num_nodes)]

        # BFS through the graph starting from every node, update can_reach with the reachable nodes
        for node in range(num_nodes):
            self.can_reach[node][node] = True  # Any node can reach itself in 0 steps
            visited = {node}
            q = [node]
            while q:
                x = q.pop(0)
                visited.add(x)
                for neighbour in neighbour_lists[x]:
                    if neighbour not in visited:
                        self.can_reach[node][neighbour] = True
                        q.append(neighbour)
