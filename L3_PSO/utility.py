from math import exp
from random import randint

from bitarray import bitarray


def random_bit_array(dimension: int) -> bitarray:
    bits = [randint(0, 1) for i in range(dimension)]

    # If we return a full mask of 0, or a full mask of 1
    # we retry the computation by calling the function again.
    if bits.count(0) == 0 or bits.count(1) == 0:
        return random_bit_array(dimension)

    return bitarray(bits)


def hamming_distance(a: bitarray, b: bitarray) -> int:
    """
    Computes the Hamming distance by xor-ing and counting the set bits in the xor result

    :param a: bitarray
    :param b: bitarray
    :return: distance: the Hamming distance between a and b
    """
    return (a ^ b).count()


def logical_xor(a: bitarray, b: bitarray, index: int) -> int:
    xor = (a ^ b)
    return int(xor[index])


def sigmoid(x) -> float:
    return 1 / (1 + exp(-x))
