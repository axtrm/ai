from particle import Particle
from problem import Problem


class Swarm:

    def __init__(self, swarm_size: int, problem: Problem):
        self.particles = [Particle(problem) for i in range(swarm_size)]

        # Compute neighbours (circular model)
        self.particles.append(self.particles[0])
        for i in range(swarm_size):
            self.particles[i].add_neighbour(self.particles[i - 1])
            self.particles[i].add_neighbour(self.particles[i + 1])

    def get_best_particle(self, problem: Problem):
        return max(self.particles, key=lambda p: p.fitness(problem))

    @staticmethod
    def get_best_neighbour(particle: Particle, problem: Problem):
        return max(particle.neighbours, key=lambda p: p.fitness(problem))

    def __iter__(self):
        return iter(self.particles)
