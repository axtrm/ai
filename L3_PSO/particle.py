from random import random

from bitarray import bitarray

from problem import Problem
from utility import random_bit_array, logical_xor, sigmoid


class Particle:

    def __init__(self, problem: Problem):
        dimension = problem.dimension
        self.position = random_bit_array(dimension)
        self.velocity = [random() for i in range(dimension)]

        self.neighbours = []
        self.best_position = self.position
        self.best_fitness = self.fitness(problem)

    def add_neighbour(self, particle):
        self.neighbours.append(particle)

    def fitness(self, problem: Problem) -> float:
        can_reach = problem.can_reach
        nodes = list(range(problem.dimension))

        return (self.compute_partial_fitness(self.get_subgraph0(nodes), can_reach) +
                self.compute_partial_fitness(self.get_subgraph1(nodes), can_reach)) / 2.0

    def evaluate(self, problem: Problem):
        f = self.fitness(problem)
        if f > self.best_fitness:
            self.best_fitness = f
            self.best_position = self.position

    def update(self, particle, w, c1, c2):
        for i in range(len(self.velocity)):
            v = w * self.velocity[i] + \
                c1 * random() * logical_xor(self.best_position, self.position, i) + \
                c2 * random() * logical_xor(particle.position, particle.best_position, i)
            self.velocity[i] = v

        # Formula from PSO course, slide 20
        # slightly modified to work a bit like crossover from EA
        for i in range(len(self.velocity)):
            if random() < sigmoid(self.velocity[i]):
                self.position[i] = particle.position[i]

    def get_subgraph0(self, s: list) -> list:
        not_mask = bitarray(self.position)
        not_mask.invert()
        return self.get_subgraph(not_mask, s)

    def get_subgraph1(self, s: list) -> list:
        return self.get_subgraph(self.position, s)

    @staticmethod
    def get_subgraph(mask: bitarray, s: list) -> list:
        block = []
        for i in range(mask.length()):
            if mask[i]:
                block += [s[i]]
        return block

    @staticmethod
    def compute_partial_fitness(subgraph, can_reach) -> float:
        # TODO: change representation of can_reach from Boolean to ints - 1 and 0
        # TODO: change this to just use sum() or reduce()
        max_connected = 0
        for x in subgraph:
            connected = 0
            for y in subgraph:
                if x != y:
                    if can_reach[x][y]:
                        connected += 1
            max_connected = max(max_connected, connected)
        return float(max_connected) / len(subgraph)

    def __repr__(self):
        return "Particle(current={}, best={})".format(self.position.to01(), self.best_position.to01())
