import configparser

from particle import Particle
from problem import Problem
from swarm import Swarm


class Controller:

    def __init__(self, problem: Problem, config_file: str):
        self.w = 0.0
        self.c1 = 0.0
        self.c2 = 0.0
        self.population_size = 0
        self.no_generations = 0

        self.load_parameters(config_file)
        self.swarm = Swarm(self.population_size, problem)

    def iteration(self, problem: Problem):
        best_particle = self.swarm.get_best_particle(problem)
        for particle in self.swarm:
            particle.update(best_particle, self.w, self.c1, self.c2)
            particle.evaluate(problem)

    def run(self, problem: Problem):
        for i in range(self.no_generations):
            self.iteration(problem)

    def get_best_particle(self, problem: Problem) -> Particle:
        return self.swarm.get_best_particle(problem)

    def load_parameters(self, config_file: str):
        config = configparser.ConfigParser()
        config.read(config_file)

        if 'constants' not in config:
            raise Exception("Invalid config file '{}'.".format(config_file))

        constants = config['constants']
        self.w = float(constants['w'])
        self.c1 = float(constants['c1'])
        self.c2 = float(constants['c2'])
        self.population_size = int(constants['population_size'])
        self.no_generations = int(constants['no_generations'])
