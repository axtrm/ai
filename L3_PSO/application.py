from statistics import mean, stdev

from controller import Controller
from particle import Particle
from problem import Problem
from utility import logical_xor

NUM_RUNS = 10


class Application:

    def run(self, input_file: str, config_file: str):
        problem = Problem(input_file)
        controller = Controller(problem, config_file)

        best_particles = []

        for i in range(NUM_RUNS):
            controller.run(problem)
            best = controller.get_best_particle(problem)
            best_particles.append(best)
            print("Best solution: {0} (with f={1})".format(best, best.fitness(problem)))

        fitness_values = [particle.fitness(problem) for particle in best_particles]

        miu = mean(fitness_values)
        sigma = stdev(fitness_values)

        print("Mean = {0}, Standard deviation = {1}".format(miu, sigma))
        self.plot(fitness_values)

    @staticmethod
    def plot(fitness_values):
        import matplotlib.pyplot as plt
        plt.plot(list(range(NUM_RUNS)), fitness_values, 'ro')
        plt.ylabel('Best fitness')
        plt.xlabel('Generations')
        plt.show()


if __name__ == '__main__':
    app = Application()
    app.run("data/data00.in", "data/params.in")
